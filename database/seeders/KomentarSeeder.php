<?php

namespace Database\Seeders;

use App\Models\Komentar;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class KomentarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Komentar::create([
            'komentar' => 'Mantap',
            'id_postingan' => 1,
            'id_komentator' => 1,
        ]);
        Komentar::create([
            'komentar' => 'KEREN',
            'id_postingan' => 1,
            'id_komentator' => 1,
        ]);
    }
}
