<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Komentar extends Model
{
    use HasFactory;
    protected $table = 'komentar';
    protected $fillable = [
        'komentar',
        'id_postingan',
        'id_komentator',

    ];

    public function postingan()
    {
        return $this->belongsTo(Postingan::class,'id_postingan');
    }

    public function komentator()
    {
        return $this->belongsTo(User::class,'id_komentator');
    }

    public static function allData()
    {
        // query builder
        return DB::table("komentar")->get();
    }
}
