<?php

namespace Database\Seeders;

use App\Models\Likes;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class LikesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $likes = [
            ['id_pengguna' => 1, 'id_postingan' => 1],
            ['id_pengguna' => 2, 'id_postingan' => 1],
            ['id_pengguna' => 3, 'id_postingan' => 1],
        ];

        foreach($likes as $item){
            Likes::create(
                [
                    'id_pengguna' => $item['id_pengguna'],
                    'id_postingan' => $item['id_postingan'],
                ]
            );
        }
    }
}
