<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Follow extends Model
{
    use HasFactory;
    protected $fillable = [
        'id_pengguna',
        'id_following',

    ];

    public function username()
    {
        return $this->belongsTo(User::class,'id_pengguna');
    }
    public function following()
    {
        return $this->belongsTo(User::class,'id_following');
    }

    public static function allData()
    {
        // query builder
        return DB::table("follows")->get();
    }
}
